package ictgradschool.industry.lab03.ex09;

public class Deodorant {

    private String brand;
    private String fragrance;
    private boolean rollOn;
    private double price;

    public Deodorant(String brand, String fragrance,
                     boolean rollOn, double price) {

        this.brand = brand;
        this.fragrance = fragrance;
        this.rollOn = rollOn;
        this.price = price;
    }

    public String toString() {
        String info = brand + " " + fragrance;
        if (rollOn) {
            info = info + " Roll-On";
        } else {
            info = info + " Spray";
        }
        info +=  " Deodorant, \n$" + price;
        return info;
    }

    // TODO Implement all methods below this line.

    public double getPrice() {
        return price;
    }

    public String getBrand() {
        return brand;
    }

    public boolean isRollOn() {
        return this.rollOn;
    }

    public String getFragrance() {
        return fragrance;
    }

    public double setPrice(double price) {
        this.price = price;
        return price;
    }

    public String setBrand(String brand) {
        this.brand = brand;
        return brand;
    }

    public String setFragrance(String fragrance) {
        this.fragrance = fragrance;
        return fragrance;

    }

    public boolean isMoreExpensiveThan(Deodorant other) {

        if (this.price>other.price){
            return true;
        }

        return false;
    }
}
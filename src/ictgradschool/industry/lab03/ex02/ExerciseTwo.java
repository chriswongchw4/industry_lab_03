package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private int lowerBound;
    private int upperBound;
    private int num1;
    private int num2;
    private int num3;
    private int minnum;


    private void start() {
        System.out.print("Lower Bound? ");
        int lowerBound = Integer.parseInt(Keyboard.readInput());

        System.out.print("Upper bound? ");
        int upperBound = Integer.parseInt(Keyboard.readInput());



        num1=(int)(lowerBound+((upperBound+1-lowerBound)*Math.random()));
        num2=(int)(lowerBound+((upperBound+1-lowerBound)*Math.random()));
        num3=(int)(lowerBound+((upperBound+1-lowerBound)*Math.random()));

        System.out.println("3 randomly generate numbers: "+ num1 +", "+ num2 +", "+  num3 +".");

        minnum=Math.min(num1,num2);
        minnum=Math.min(minnum,num3);

        System.out.println("Smallest number is "+ minnum);



    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
